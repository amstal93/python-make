ARG DOCKER_FROM
FROM $DOCKER_FROM

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
      git \
      make \
      python3-pip \
      python3-venv \
    && rm -fr /var/lib/apt/lists/* \
    && rm -fr /usr/share/man/* /usr/share/doc/* /usr/share/info/* \
    ;


